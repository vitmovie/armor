package connection;

import java.sql.*;
import java.util.Locale;

public class connection {

    private static final String INSERT_NEW = "INSERT INTO firstable VALUES(?,?)";
    private static final String SELECT_MAX_PK = "SELECT MAX(FIRSTABLE_PK) as MAX_PK FROM FIRSTABLE";
    private static final String SELECT_ALL_ELEM = "select firstable_pk, treat(myobj as obj).col1, treat(myobj as obj).col2, treat(myobj as obj).col3 from firstable";

    public static void main(String[] argv) {
        
        Locale.setDefault(Locale.ENGLISH);
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return;
        }

        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:XE", "system", "qwerty");
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }

        int max_pk = 0;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SELECT_MAX_PK);
            if(rs.next()) {
                max_pk = rs.getInt("MAX_PK");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("MAX PK IN FIRSTABLE: " + max_pk);
        PreparedStatement preparedStatement = null;
        try {
            for(int i = 0; i < 10; i++) {
                Struct obj = connection.createStruct("OBJ", new Object[]{i + 1, "prShort", "prInfo"});
                preparedStatement = connection.prepareStatement(INSERT_NEW);
                preparedStatement.setInt(1, max_pk + i + 1);
                preparedStatement.setObject(2, obj);
                preparedStatement.executeUpdate();
                System.out.println(max_pk + i + 1 + " New object added to FIRSTABLE!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement all = null;
        try {
            all = connection.createStatement();
            ResultSet allRs = all.executeQuery(SELECT_ALL_ELEM);
            while(allRs.next())
                System.out.println(allRs.getInt(1) + " | " + allRs.getInt(2) + "," + allRs.getString(3) + "," + allRs.getString(4));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
