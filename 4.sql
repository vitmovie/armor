CREATE SEQUENCE name_of_sequence
  START WITH 1
  INCREMENT BY 1
  CACHE 100;

create procedure pushdata as
  i number := 0;
begin
while i < 10 loop
   insert into firstable values(name_of_sequence.nextval,obj(i,'short','info'));
   i:=i+1;
end loop;
end;

begin 
    pushdata;
end;

select firstable_pk, treat(myobj as obj).col1, treat(myobj as obj).col2, treat(myobj as obj).col3 from firstable



  